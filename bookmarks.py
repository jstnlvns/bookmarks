from getpass import getpass
import re
import sys
import requests
import json
import os
import sys
import shutil
import argparse

from jinja2 import Environment, FileSystemLoader

basepath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
BASEURL = "https://jstnlvns.gitlab.io/bookmarks/"
DESTINATION = "public/"
LAYOUTSOURCE = "layouts/"
LAYOUTS = {
	"base": "base.html",
	"post": "post.html",
	"home": "home.html",
	"tags": "tags.html",
	"bookmarktags": "bookmarks_by_tag.html"
}

file_loader = FileSystemLoader(LAYOUTSOURCE)
env = Environment(loader=file_loader)

def getPosts(token):
	api_url = 'https://api.pinboard.in/v1/'
	payload = {"auth_token": token}
	call = f"{api_url}posts/all?format=json"
	results = requests.get(call, params=payload)
	resulttxt = results.text.encode().decode('utf-8-sig')
	resultsjson = json.loads(resulttxt)
	bookmarks = []
	for post in resultsjson:
		tags = post['tags'].split(" ")
		if 'Bookmark' in tags:
			bookmark_entry = {}
			bookmark_entry['href'] = post['href']
			bookmark_entry['description'] = post['description']
			bookmark_entry['tags'] = post['tags']
			bookmarks.append(bookmark_entry)
	with open("pinboard_bookmarks.json", "w") as jsonout:
		json.dump(bookmarks, jsonout, indent=4)


def processBookmark(bookmark, unique_tag_list, main_tag_list):
	bm_dict = {}
	bm_dict['url'] = bookmark['href']
	bm_dict['name'] = bookmark['description']
	post_tags = bookmark['tags'].split(" ")
	del post_tags[post_tags.index('Bookmark')]
	sorted_tags = sorted(post_tags)
	bm_dict['tags'] = sorted_tags
	for tag in sorted_tags:
		if 'main' in tag and tag not in main_tag_list:
			main_tag_list.append(tag)
		if 'main' not in tag and tag not in unique_tag_list:
			unique_tag_list.append(tag)
	return bm_dict, unique_tag_list, main_tag_list


def getBookmarks():
	unique_tag_list = []
	main_tag_list = []
	bookmarks = []
	with open("pinboard_bookmarks.json", "r") as jsonin:
		posts = json.load(jsonin)
		for post in posts:
			bookmark, unique_tag_list, main_tag_list = processBookmark(post, unique_tag_list, main_tag_list)
			bookmarks.append(bookmark.copy())
	return bookmarks, unique_tag_list, main_tag_list


def orgByMain(bookmarks, main_tag_list):
	sorted_bookmarks = sorted(bookmarks, key=lambda item: item.get("name").casefold())
	main_tag_list = sorted(main_tag_list)
	mainOrgs = {}
	for tag in main_tag_list:
		mainOrgs[tag] = []
	for bookmark in sorted_bookmarks:
		for tag in bookmark['tags']:
			if tag in main_tag_list:
				mainOrgs[tag].append(bookmark)
		for tag in main_tag_list:
			if tag in bookmark['tags']:
				main_index = bookmark['tags'].index(tag)
		del bookmark['tags'][main_index]
		tag_urls = []
		for tag in bookmark['tags']:
			tag_url = f"<a href=\"tags/{tag}/index.html\">{tag}</a>"
			tag_urls.append(tag_url)
		bookmark['tag_urls'] = tag_urls
	pretty_dict = {}
	for main_tag, bookmarks in mainOrgs.items():
		if "|" in main_tag:
			pretty_main_list = main_tag.split("|")
			pretty_main = f"{pretty_main_list[0].split(':')[-1].title()} | {pretty_main_list[1].upper()}"
		else:
			pretty_main = main_tag.split(":")[-1].title()
		pretty_dict[pretty_main]= bookmarks
	return pretty_dict


def writefile(url_name, data):
    path = f'{DESTINATION}{url_name}'
    file = open(path, 'w')
    file.write(data)
    file.close()


def generate_bookmark_source_json():
	pb_api_token_input = getpass("Pinbaord API Token: ")
	api_pat = re.compile("^\S+:\S+")
	api_pat_match = api_pat.search(pb_api_token_input.strip())
	if api_pat_match:
		pb_api_token = api_pat_match.group()
	else:
		print("Your input does not match the expected format\nShould be 'username:key'")
		sys.exit()
	getPosts(pb_api_token)


def main():
	parser = argparse.ArgumentParser()
	parser.add_argument('-i', '--initialize', help="Generate Source JSON of Bookmarks from Pinboard", action="store_true", dest='initialize')
	args = parser.parse_args()
	if args.initialize:
		generate_bookmark_source_json()
	bookmarks, unique_tag_list, main_tag_list = getBookmarks()
	unique_tag_list = sorted(unique_tag_list)

	if os.path.exists("index.html"):
	    os.remove("index.html")

	index_main_bookmarks = orgByMain(bookmarks, main_tag_list)
	index_main_template = env.get_template(LAYOUTS['home'])
	print(f"Rendering index.html with rooturl={BASEURL}")
	index_main_output = index_main_template.render(bookmarks=index_main_bookmarks, rooturl=BASEURL)
	writefile('index.html', index_main_output)
	
	tags_base_directory = f"{DESTINATION}tags"
	if os.path.exists(tags_base_directory):
		shutil.rmtree(tags_base_directory)
	os.mkdir(tags_base_directory)

	tag_main_template = env.get_template(LAYOUTS['tags'])
	tag_main_output = tag_main_template.render(tags=unique_tag_list, rooturl=BASEURL)
	writefile("tags/index.html", tag_main_output)

	bookmark_template = env.get_template(LAYOUTS['bookmarktags'])
	for tag in unique_tag_list:
		tag_bookmarks = []
		tagdir = f"{tags_base_directory}/{tag}"
		os.mkdir(tagdir)
		bookmark_file = f"tags/{tag}/index.html"
		for bookmark in bookmarks:
			bookmark_tags = bookmark['tags']
			if tag in bookmark_tags:
				tag_bookmarks.append(bookmark)
		bookmark_output = bookmark_template.render(bookmarks=tag_bookmarks, rooturl=BASEURL, tagname=tag)
		writefile(bookmark_file, bookmark_output)

		
		
if __name__ == "__main__":
	main()
